package org.jastadd.ag.ast;

import java.util.*;
/**
 * @ast class
 * @aspect RefResolverHelpers
 * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:136
 */
 class Unresolved$Pillar extends Pillar implements Unresolved$Node$Interface {
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:137
   */
  
    private String unresolved$Token;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:138
   */
  
    public String getUnresolved$Token() {
      return unresolved$Token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:141
   */
  
    void setUnresolved$Token(String token) {
      this.unresolved$Token = token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:144
   */
  
    private boolean unresolved$ResolveOpposite;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:145
   */
  
    public boolean getUnresolved$ResolveOpposite() {
      return unresolved$ResolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:148
   */
  
    void setUnresolved$ResolveOpposite(boolean resolveOpposite) {
      this.unresolved$ResolveOpposite = resolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:155
   */
  Unresolved$Node$Interface as$Unresolved() {
    return this;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:161
   */
  boolean is$Unresolved() {
    return true;
  }

}
