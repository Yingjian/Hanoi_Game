package org.jastadd.ag.ast;

import java.util.*;
/**
 * @ast class
 * @aspect RefResolverHelpers
 * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:108
 */
 class Unresolved$Disk extends Disk implements Unresolved$Node$Interface {
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:109
   */
  
    private String unresolved$Token;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:110
   */
  
    public String getUnresolved$Token() {
      return unresolved$Token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:113
   */
  
    void setUnresolved$Token(String token) {
      this.unresolved$Token = token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:116
   */
  
    private boolean unresolved$ResolveOpposite;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:117
   */
  
    public boolean getUnresolved$ResolveOpposite() {
      return unresolved$ResolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:120
   */
  
    void setUnresolved$ResolveOpposite(boolean resolveOpposite) {
      this.unresolved$ResolveOpposite = resolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:127
   */
  Unresolved$Node$Interface as$Unresolved() {
    return this;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:133
   */
  boolean is$Unresolved() {
    return true;
  }

}
