package org.jastadd.ag.ast;

import java.util.*;
/**
 * @ast class
 * @aspect RefResolverHelpers
 * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:80
 */
 class Unresolved$Hanoi extends Hanoi implements Unresolved$Node$Interface {
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:81
   */
  
    private String unresolved$Token;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:82
   */
  
    public String getUnresolved$Token() {
      return unresolved$Token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:85
   */
  
    void setUnresolved$Token(String token) {
      this.unresolved$Token = token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:88
   */
  
    private boolean unresolved$ResolveOpposite;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:89
   */
  
    public boolean getUnresolved$ResolveOpposite() {
      return unresolved$ResolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:92
   */
  
    void setUnresolved$ResolveOpposite(boolean resolveOpposite) {
      this.unresolved$ResolveOpposite = resolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:99
   */
  Unresolved$Node$Interface as$Unresolved() {
    return this;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:105
   */
  boolean is$Unresolved() {
    return true;
  }

}
