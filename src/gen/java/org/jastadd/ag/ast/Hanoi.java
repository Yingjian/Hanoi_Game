/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\1\\src\\gen\\jastadd\\ag.ast:1
 * @astdecl Hanoi : ASTNode ::= Pillar* <Num:int>;
 * @production Hanoi : {@link ASTNode} ::= <span class="component">{@link Pillar}*</span> <span class="component">&lt;Num:{@link int}&gt;</span>;

 */
public class Hanoi extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect Initialisation
   * @declaredat E:\\project\\1\\src\\main\\jastadd\\hanoi\\Initialisation.jrag:3
   */
  public boolean Initialisation(int sumDisk)
  {
    this.setNum(sumDisk);
    int sumPillar = 3;
    Disk[] D = new Disk[sumDisk];
    Pillar[] P = new Pillar[3];
    for(int i = 0; i < 3; i++)
    {
      P[i] = new Pillar();
    }
    for(int i = 0; i < sumDisk; i++)
    {
      D[i] = new Disk(i + 1);
      System.out.println("Disk: " + i + "; size: " + D[i].getSize());
    }
    for(int i = sumDisk - 1; i >= 0; i--)
    {
      P[0].addDisk(D[i]);
    }
    for(int i = 0; i < 3; i++)
    {
      this.addPillar(P[i]);
    } 
    this.present();
    return true;
  }
  /**
   * @aspect Initialisation
   * @declaredat E:\\project\\1\\src\\main\\jastadd\\hanoi\\Initialisation.jrag:29
   */
  public boolean game(){
    boolean turn = true;
    int count = 1;
    int location = 0;
    while(true){
      if(this.getPillar(2).getNumDisk() == this.getNum()){
        break;
      }
      if(turn == true){
        int temp = this.getPillar(location).moveInSeq();
        System.out.println("Round: " + count++);
        location = temp;
        turn = false;
      }
      else for(int i = 0; i < 3; i++){
        if(i != location && turn == false){
          for(Pillar P : this.getPillar(i).successors()){
            if(this.getPillar(i).moveTo(P)){
              System.out.println("Disk_" + (P.getDisk(P.getNumDisk()-1).getSize()-1) + ": " + i + "->" + P.IdinHanoi());
              System.out.println("Round: " + count++);
              turn = true;
              break;
            }
          }
        }
      }
    }
    return true;
  }
  /**
   * @aspect Initialisation
   * @declaredat E:\\project\\1\\src\\main\\jastadd\\hanoi\\Initialisation.jrag:58
   */
  public boolean present()
  {
    int sumPillar = 3;
    for(int i = 0; i < 3; i++)
    {
      if(this.getPillar(i).getNumDisk() > 0){
        System.out.println("Pillar_" + i + " has Disks:");
        for(int j = 0; j < this.getNum(); j++){
          int temp = this.getPillar(i).getDisk(j).getSize();
          System.out.println("Disk: " + (temp - 1) + "; Size: " + temp + ".");
        }
      }
      else{
        System.out.println("Pillar_" + i + " is empty.");
      }
    }
    return true; 
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:3
   */
  public static Hanoi createRef(String ref) {
    Unresolved$Hanoi unresolvedNode = new Unresolved$Hanoi();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:9
   */
  public static Hanoi createRefDirection(String ref) {
    Unresolved$Hanoi unresolvedNode = new Unresolved$Hanoi();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:60
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:96
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\1\\src\\gen\\jastadd\\agRefResolver.jadd:102
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Hanoi() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[1];  getChild_handler = new ASTNode$DepGraphNode[children.length];
    state().enterConstruction();
    setChild(new JastAddList(), 0);
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:16
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Pillar", "Num"},
    type = {"JastAddList<Pillar>", "int"},
    kind = {"List", "Token"}
  )
  public Hanoi(JastAddList<Pillar> p0, int p1) {
state().enterConstruction();
    setChild(p0, 0);
    setNum(p1);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:28
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:36
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  public Hanoi clone() throws CloneNotSupportedException {
    Hanoi node = (Hanoi) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:51
   */
  public Hanoi copy() {
    try {
      Hanoi node = (Hanoi) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:75
   */
  @Deprecated
  public Hanoi fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:85
   */
  public Hanoi treeCopyNoTransform() {
    Hanoi tree = (Hanoi) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:106
   */
  public Hanoi treeCopy() {
    Hanoi tree = (Hanoi) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:121
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:124
   */
  protected void inc_copyHandlers(Hanoi copy) {
    super.inc_copyHandlers(copy);

        if (getNum_handler != null) {
          copy.getNum_handler = ASTNode$DepGraphNode.createAstHandler(getNum_handler, copy);
        }
  }
  /** @apilevel internal 
   * @declaredat ASTNode:133
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:140
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:142
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  if (getNum_handler != null) {
    getNum_handler.throwAway();
  }
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:154
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:155
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  if (getNum_handler != null) {
    getNum_handler.cleanupListeners();
  }
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:166
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:167
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   * Replaces the Pillar list.
   * @param list The new list node to be used as the Pillar list.
   * @apilevel high-level
   */
  public Hanoi setPillarList(JastAddList<Pillar> list) {
    setChild(list, 0);
    return this;
  }
  /**
   * Retrieves the number of children in the Pillar list.
   * @return Number of children in the Pillar list.
   * @apilevel high-level
   */
  public int getNumPillar() {
    return getPillarList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Pillar list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Pillar list.
   * @apilevel low-level
   */
  public int getNumPillarNoTransform() {
    return getPillarListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Pillar list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Pillar list.
   * @apilevel high-level
   */
  public Pillar getPillar(int i) {
    return (Pillar) getPillarList().getChild(i);
  }
  /**
   * Check whether the Pillar list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasPillar() {
    return getPillarList().getNumChild() != 0;
  }
  /**
   * Append an element to the Pillar list.
   * @param node The element to append to the Pillar list.
   * @apilevel high-level
   */
  public Hanoi addPillar(Pillar node) {
    JastAddList<Pillar> list = (parent == null) ? getPillarListNoTransform() : getPillarList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public Hanoi addPillarNoTransform(Pillar node) {
    JastAddList<Pillar> list = getPillarListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the Pillar list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public Hanoi setPillar(Pillar node, int i) {
    JastAddList<Pillar> list = getPillarList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the Pillar list.
   * @return The node representing the Pillar list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Pillar")
  public JastAddList<Pillar> getPillarList() {
    JastAddList<Pillar> list = (JastAddList<Pillar>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Pillar list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Pillar list.
   * @apilevel low-level
   */
  public JastAddList<Pillar> getPillarListNoTransform() {
    return (JastAddList<Pillar>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Pillar list without
   * triggering rewrites.
   */
  public Pillar getPillarNoTransform(int i) {
    return (Pillar) getPillarListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Pillar list.
   * @return The node representing the Pillar list.
   * @apilevel high-level
   */
  public JastAddList<Pillar> getPillars() {
    return getPillarList();
  }
  /**
   * Retrieves the Pillar list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Pillar list.
   * @apilevel low-level
   */
  public JastAddList<Pillar> getPillarsNoTransform() {
    return getPillarListNoTransform();
  }
  /**
   */
  protected ASTNode$DepGraphNode getNum_handler = ASTNode$DepGraphNode.createAstHandler(this, "getNum", null);
  /**
   * Replaces the lexeme Num.
   * @param value The new value for the lexeme Num.
   * @apilevel high-level
   */
  public Hanoi setNum(int value) {
    tokenint_Num = value;
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      getNum_handler.notifyDependencies();
    
    
    
    
    }
    return this;
  }
  /** @apilevel internal 
   */
  protected int tokenint_Num;
  /**
   * Retrieves the value for the lexeme Num.
   * @return The value for the lexeme Num.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Num")
  public int getNum() {
    
    state().addHandlerDepTo(getNum_handler);
    return tokenint_Num;
  }
  /**
   * @declaredat E:\\project\\1\\src\\main\\jastadd\\hanoi\\CanMove.jrag:15
   * @apilevel internal
   */
  public Set<Pillar> Define_successors(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getPillarListNoTransform()) {
      // @declaredat E:\\project\\1\\src\\main\\jastadd\\hanoi\\CanMove.jrag:16
      int i = _callerNode.getIndexOfChild(_childNode);
      {
          Set<Pillar> result = new HashSet<Pillar>();
          for (Pillar P : getPillars()) {
            if (P != this.getPillar(i)) result.add(P);
          }
            return result;
        }
    }
    else {
      return getParent().Define_successors(this, _callerNode);
    }
  }
  /**
   * @declaredat E:\\project\\1\\src\\main\\jastadd\\hanoi\\CanMove.jrag:15
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute successors
   */
  protected boolean canDefine_successors(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat E:\\project\\1\\src\\main\\jastadd\\hanoi\\CanMove.jrag:24
   * @apilevel internal
   */
  public int Define_IdinHanoi(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getPillarListNoTransform()) {
      // @declaredat E:\\project\\1\\src\\main\\jastadd\\hanoi\\CanMove.jrag:25
      int i = _callerNode.getIndexOfChild(_childNode);
      {
          return i;
        }
    }
    else {
      return getParent().Define_IdinHanoi(this, _callerNode);
    }
  }
  /**
   * @declaredat E:\\project\\1\\src\\main\\jastadd\\hanoi\\CanMove.jrag:24
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute IdinHanoi
   */
  protected boolean canDefine_IdinHanoi(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat E:\\project\\1\\src\\main\\jastadd\\hanoi\\Moveable.jrag:3
   * @apilevel internal
   */
  public int Define_moveInSeq(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getPillarListNoTransform()) {
      // @declaredat E:\\project\\1\\src\\main\\jastadd\\hanoi\\Moveable.jrag:4
      int i = _callerNode.getIndexOfChild(_childNode);
      {
          int temp = this.getPillar(i).moveSeq();
          boolean check = this.getPillar(i).moveTo(this.getPillar(temp));
          System.out.println("Disk_0: " + i + "->" + temp);
          return temp; 
        }
    }
    else {
      return getParent().Define_moveInSeq(this, _callerNode);
    }
  }
  /**
   * @declaredat E:\\project\\1\\src\\main\\jastadd\\hanoi\\Moveable.jrag:3
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute moveInSeq
   */
  protected boolean canDefine_moveInSeq(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat E:\\project\\1\\src\\main\\jastadd\\hanoi\\Moveable.jrag:10
   * @apilevel internal
   */
  public int Define_sumDisk(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getPillarListNoTransform()) {
      // @declaredat E:\\project\\1\\src\\main\\jastadd\\hanoi\\Moveable.jrag:11
      int i = _callerNode.getIndexOfChild(_childNode);
      {
          int j = this.getNum();
          return j;
        }
    }
    else {
      return getParent().Define_sumDisk(this, _callerNode);
    }
  }
  /**
   * @declaredat E:\\project\\1\\src\\main\\jastadd\\hanoi\\Moveable.jrag:10
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute sumDisk
   */
  protected boolean canDefine_sumDisk(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat E:\\project\\1\\src\\main\\jastadd\\hanoi\\Moveable.jrag:15
   * @apilevel internal
   */
  public int Define_moveSeq(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getPillarListNoTransform()) {
      // @declaredat E:\\project\\1\\src\\main\\jastadd\\hanoi\\Moveable.jrag:16
      int i = _callerNode.getIndexOfChild(_childNode);
      {
          if(this.getNum()%2 == 1){
            return (i+2)%3;
          }else{
            return (i+1)%3;
          }
        }
    }
    else {
      return getParent().Define_moveSeq(this, _callerNode);
    }
  }
  /**
   * @declaredat E:\\project\\1\\src\\main\\jastadd\\hanoi\\Moveable.jrag:15
   * @apilevel internal
   * @return {@code true} if this node has an equation for the inherited attribute moveSeq
   */
  protected boolean canDefine_moveSeq(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
